% \documentclass{article}
% \usepackage{amsmath, amssymb, amsfonts, amsthm}
% \usepackage{graphicx}
% \usepackage{natbib}
% \usepackage{color}
% \usepackage{bm}
% \usepackage{subfigure}
% \usepackage{graphicx}
% \usepackage{mathabx}
% \usepackage{multirow}
% \usepackage{setspace}
% \usepackage{fancyref}
% \usepackage{microtype}
% \usepackage{units}
% \usepackage{siunitx}
% %\usepackage{cite}  % If you include this, hyperlink cites will
%                      % break.  It's nice to use this package if your bibstyle
% 							% sorts entries by order-of-use, rather than
% 							% alphabetically (as plain does).
							
% %Theorem, Lemma, etc. environments
% \newtheorem{theorem}{Theorem}%[section]
% \newtheorem{lemma}[theorem]{Lemma}
% \newtheorem{proposition}[theorem]{Proposition}
% \newtheorem{corollary}[theorem]{Corollary}
% \newtheorem{result}[theorem]{Result}

% % Personal commands and abbreviations.
% \renewcommand{\labelenumi}{(\alph{enumi})} % Use letters for enumerate
% % \DeclareMathOperator{\Sample}{Sample}
% \let\vaccent=\v % rename builtin command \v{} to \vaccent{}
% \renewcommand{\v}[1]{\ensuremath{\mathbf{#1}}} % for vectors
% \newcommand{\gv}[1]{\ensuremath{\mbox{\boldmath$ #1 $}}} 
% % for vectors of Greek letters
% \newcommand{\uv}[1]{\ensuremath{\mathbf{\hat{#1}}}} % for unit vector
% \newcommand{\abs}[1]{\left| #1 \right|} % for absolute value
% \newcommand{\avg}[1]{\left< #1 \right>} % for average
% \let\underdot=\d % rename builtin command \d{} to \underdot{}
% \renewcommand{\d}[2]{\frac{d #1}{d #2}} % for derivatives
% \newcommand{\dd}[2]{\frac{d^2 #1}{d #2^2}} % for double derivatives
% \newcommand{\pd}[2]{\frac{\partial #1}{\partial #2}} 
% % for partial derivatives
% \newcommand{\pdd}[2]{\frac{\partial^2 #1}{\partial #2^2}} 
% % for double partial derivatives
% \newcommand{\pdc}[3]{\left( \frac{\partial #1}{\partial #2}
%  \right)_{#3}} % for thermodynamic partial derivatives
% \newcommand{\ket}[1]{\left| #1 \right>} % for Dirac bras
% \newcommand{\bra}[1]{\left< #1 \right|} % for Dirac kets
% \newcommand{\braket}[2]{\left< #1 \vphantom{#2} \right|
%  \left. #2 \vphantom{#1} \right>} % for Dirac brackets
% \newcommand{\matrixel}[3]{\left< #1 \vphantom{#2#3} \right|
%  #2 \left| #3 \vphantom{#1#2} \right>} % for Dirac matrix elements
% \newcommand{\grad}[1]{\gv{\nabla} #1} % for gradient
% \let\divsymb=\div % rename builtin command \div to \divsymb
% \renewcommand{\div}[1]{\gv{\nabla} \cdot #1} % for divergence
% \newcommand{\curl}[1]{\gv{\nabla} \times #1} % for curl
% \let\baraccent=\= % rename builtin command \= to \baraccent
% \renewcommand{\=}[1]{\stackrel{#1}{=}} % for putting numbers above =
%  \newcommand{\Ei}{\operatorname{Ei}}
% \newcommand{\intd}{\operatorname{ \ \mathrm{d}}}
% %Define and personal commands here

% \graphicspath{{../Pictures/}}
% \title{Kinematics}
% \author{zack.scholl }
% \date{April 2016}

% \begin{document}

% \maketitle

\section{Introduction}

We assume that the rupture event is a quasi-adiabatic process, which implies that the characteristic time of relaxation of the system in the folded state is much faster than the escape over the barrier. The probability that a molecule undergoing a transition is simply
\begin{equation}
\frac{d P_{folded}(t)}{d t} = - k_{unf} P_{folded}(t)
\end{equation} 
where $P_{folded}(t)$ is the cumulative probability distribution for the probability that a molecule remains folded at time $t$ and $k_{unf}$ is the characteristic unfolding rate.

However, when the molecule is further perturbed by force, then the unfolding rate becomes dependent on the force, $k_{unf}=k_{unf}(F(t))$,  so 
\begin{equation}\label{eq:dp_eq_kP}
\frac{d P_{folded}(t)}{d t} = - k_{unf}(F(t)) P_{folded}(t).
\end{equation}


It is useful to use a change of variables to collect these quantities in terms of the force, $F(t)$, which is measured by the AFM and also of importance to the molecular state so the equation becomes
\begin{equation}\label{eq:dp_eq_kP_F}
\frac{\partial P_{folded}(F(t))}{\partial F} \frac{\partial F}{\partial t} = -k_{unf}(F(t)) P_{folded}(F(t)).
\end{equation} 

\subsection{Force-spectroscopy experiment measurements}

Before proceeding with the kinetics, its useful to understand the mathematics behind the constant-velocity force-spectroscopy experiments. In this experiment, a polyprotein is being pulled by a cantilever while the cantilever moves away from the substrate at constant velocity. 

There are two observables from the AFM experiment: firstly, there is the distance between the base of the cantilever and the surface of the substrate as measured by the piezo electric device $z(\hat{t})$, and secondly, the deflection of the cantilever as it flucuates or is perturbed by a resistant protein, $\delta (\hat{t})$. Both of these observables are recorded over the course of the experimental time, $\hat{t}$. Note that this experimental time $\hat{t}$ is not the same as the time $t$ in equation \ref{eq:dp_eq_kP_F}. The experimental time increases monotonically during the experiment, while the time $t$ is specific lifetime of the molecule of interest which may change during the entire course of an experiment.

When the experiment starts, at $\hat{t}=0$, the base of the cantilever moves with velocity $v$ until the experiment reaches a specified relative distance from the substrate, $z(\hat{t})$. The distance between the base of the cantilever and the substrate is always given by simply
\begin{equation}\label{eq:z_vt}
z(\hat{t}) = v \hat{t}.
\end{equation} 

The measured deflection, $\delta (\hat{t})$, can be converted to the biologically relevant force using Hooke's law,
\begin{equation}\label{eq:hookes_law1}
F(\hat{t}) = k_c \delta(\hat{t}) 
\end{equation} 
where $k_c$ is the spring constant of the cantilever in use.

When a molecule is adhered to the cantilever tip and attached to the substrate, it will resist the force according to its physical properties. A simple polymer will balance the entropic and enthalpic and external forces so that it extends a force-dependent amount, $x(F(\hat{t}))$. This molecule will thus affect the deflection of the cantilever, however the molecular extension and the deflection of the cantilever must always sum to the total relative distance between the base of the cantilever and the substrate,

\begin{equation}\label{eq:z_delta_x}
z(\hat{t}) = \delta(\hat{t}) + x(F(\hat{t})).
\end{equation}

\subsection{Derivation of the loading rate}

In order to solve for the Markovian kinetics of the folding probabilities in equation \ref{eq:dp_eq_kP_F}, we will need a solution to the loading rate, $ \frac{\partial F}{\partial \hat{t}} $. It can be given simply using equation \ref{eq:hookes_law1}, and then substituting equation \ref{eq:z_delta_x} and equation \ref{eq:z_vt} to get

\begin{equation}\label{eq:df_kc_ddelta}
\frac{\partial F}{\partial \hat{t}} = k_c \frac{\partial  \delta(\hat{t})}{\partial \hat{t}} 
 = k_c \left( \frac{\partial z(\hat{t})}{\partial \hat{t}} - \frac{\partial x\left( F(\hat{t}) \right)}{\partial \hat{t}} \right) = k_c \left( \hat{t} - \frac{\partial x\left( F \right)}{\partial F}  \frac{\partial F}{\partial \hat{t}}\right).
\end{equation} 

As can be seen, if molecular extension is present then the loading rate will be a function of the loading rate itself. Rearranging equation \ref{eq:df_kc_ddelta} gives

\begin{equation}\label{eq:loading_rate1}
\frac{\partial F}{\partial \hat{t}} = v \left( \frac{1}{k_c} + \frac{\partial x\left( F \right)}{\partial F} \right)^{-1}
\end{equation}
which is the force-balanced equation of the loading-rate at constant velocity. For a typical protein, the linkers in the protein tend to behave like a worm-like chain so that the function $ x\left( F \right)$ is known, but analytically intractable. However, there exists a highly accurate approximation that can be used for molecular extensions that behave like a worm-like chain \cite{Dudko2006},
\begin{equation}\label{eq:loading_rate2}
\frac{\partial F}{\partial \hat{t}} = v \left[ \frac{1}{k_c} + \frac{2 \beta L_c p(1 + \beta F p)}{3 + 5 \beta F p + 8 (\beta F p)^{5/2}} \right]^{-1}
\end{equation}
where the parameters are associated with the worm-like chain: $\beta$ for $1/k_BT$, $p$ for the persistence length, and $L_c$ for the contour-length.


\subsection{Force-spectrosocpy observables}

The utility of equation \ref{eq:dp_eq_kP_F} lies in finding force-spectroscopy observables that can be used to describe the probability distributions. In fact, as pointed out by references \cite{evans2009new,Evans2010} and later by \cite{Zhang2013}, there is a clever rearrangement of equation \ref{eq:dp_eq_kP_F} that will create this utility. To see this, we can first solve for the unfolding rate in equation \ref{eq:dp_eq_kP_F},

\begin{equation}\label{eq:dp_eq_kP_F2}
k_{unf}(F) =  - \frac{1}{P_{folded}(F)} \left( \frac{\partial P_{folded}(F)}{\partial F} \frac{\partial F}{\partial t} \right).
\end{equation} 

The probability of remaining folded at some force, $P_{folded}(F)$, is directly related to a experimental observable. In fact, a set of experiments will yield hundreds of rupture forces. One can sort the rupture forces in increasing order and then compute the inverse of the estimated cumulative distribution across bins of size $\Delta F$ so that
\begin{equation}\label{eq:pf_nf_ntotal}
P_{folded}(F) = N_{folded}(F_k)/N_{total}
\end{equation} 
where $N_{folded}(F_k)$ are the number of molecules that are still folded in force bin $F_k$ and $N_{total}$ is the total number of rupture forces.

The change in probability of being folded, $\frac{\partial P_{folded}(F)}{\partial F}$ can find an experimental observable by transfomration to probability density. Note that  $\frac{\partial P_{folded}(F)}{\partial F}$ is equivalent to the $\frac{\partial (1 - P_{unfolded}(F))}{\partial F}$ and that by definition the derivative of a cumulative profitability distribution is equal to the probability density, so that 
\begin{equation}\label{eq:pf_punf}
\frac{\partial P_{folded}(F)}{\partial F} = \frac{\partial (1 - P_{unfolded}(F))}{\partial F} = -\frac{ \partial P_{unfolded}(F)}{\partial F} = - p_{unfolded}(F)
\end{equation} 
where $p_{unfolded}(F)$ is the probability density of being unfolded at force $F$. This can be given simply by a normalized histogram of unfolding forces such that
\begin{equation}\label{eq:pf_punf2}
p_{unfolded}(F) = \frac{N_{unfolded}(F_k)}{ N_{total} \Delta F }
\end{equation} 
where $N_{unfolded}(F_k)$ are the number of rupture events that fall into bin $F_k$ with a bin size of $\Delta F$.

Finally, combining equations \ref{eq:pf_punf2}, \ref{eq:pf_punf} and \ref{eq:pf_nf_ntotal} into \ref{eq:dp_eq_kP_F2} gives a simple equation for force-dependent loading rate,
\begin{equation}
k_{unf}(F) = \frac{N_{unfolded}(F_k) }{N_{folded}(F_k) \Delta F } \frac{\partial F}{\partial t}
\end{equation}
which is entirely dependent on force-spectroscopy observables from constant-velocity experiments.


% \bibliographystyle{../Bibliography/jasa} %Formats bibliography
% \cleardoublepage
% \normalbaselines %Fixes spacing of bibliography
% \addcontentsline{toc}{chapter}{Bibliography} %adds Bibliography to your table of contents
% \bibliography{../Bibliography/References} %your bibliography file - change the path if needed
% \end{document}
