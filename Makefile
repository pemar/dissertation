filename=dissertation

pdflatex:
	pdflatex ${filename}.tex
	pdflatex ${filename}.tex
	bibtex ${filename}
	pdflatex ${filename}.tex
	pdflatex ${filename}.tex

appendix:
	pdftk dissertation.pdf cat 229-268 output appendix.pdf

fast:
	pdflatex ${filename}.tex

pdf: ps
	ps2pdf ${filename}.ps

pdf-print: ps
	ps2pdf -dColorConversionStrategy=/LeaveColorUnchanged -dPDFSETTINGS=/printer ${filename}.ps

text: html
	html2text -width 100 -style pretty ${filename}/${filename}.html | sed -n '/./,$$p' | head -n-2 >${filename}.txt

html:
	@#latex2html -split +0 -info "" -no_navigation ${filename}
	htlatex ${filename}

ps:	dvi
	dvips -t letter ${filename}.dvi

dvi:
	latex ${filename}
	bibtex ${filename}||true
	latex ${filename}
	latex ${filename}

read:
	evince ${filename}.pdf &

aread:
	acroread ${filename}.pdf

conclusion:
	pdftk dissertation.pdf cat 214-219 output conclusion.pdf

intro:
	pdftk dissertation.pdf cat 20-28 output intro.pdf

afm:
	pdftk dissertation.pdf cat 56-64 output afm.pdf

clean:
	rm -f ${filename}.ps
	rm -f ${filename}.pdf
	rm -f ${filename}.log
	rm -f ${filename}.aux
	rm -f ${filename}.out
	rm -f ${filename}.dvi
	rm -f ${filename}.bbl
	rm -f ${filename}.blg

